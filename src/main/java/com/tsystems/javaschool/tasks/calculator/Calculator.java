package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (isCorrect(statement)){
            try {

                return getResult(solveStatement(getModifiedStatement(statement)));
            } catch (Exception e) {
                return null;
            }
        }else{
            return null;
        }
    }

    /**
     *
     * @param c symbol from the statement
     * @return true - if it is a delimiter, otherwise - false
     */
    private static boolean isDelimiter(char c){
        return c == ' ';
    }


    /**
     *
     * @param c symbol from the statement
     * @return true - if it is a operator, otherwise - false
     */
    private static boolean isOperator(char c){
        return c == '+' || c == '-' || c == '/' || c == '*' || c == '(' || c == ')';
    }


    /**
     *
     * @param operand it is mathematical operand
     * @return priority of operand from 0 to 2, default value is -1
     */
    private static byte getPriority(char operand){
        switch (operand){
            case('('):
            case(')'):
                return 0;
            case('+'):
            case('-'):
                return 1;
            case('/'):
            case('*'):
                return 2;
            default:
                return -1;
        }
    }

    /**
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                        parentheses, operations signs '+', '-', '*', '/'<br>
     * @return  string (statement) in Reverse Polish notation
     */
    private static String getModifiedStatement(String statement){
        StringBuilder modifiedStatement = new StringBuilder();
        LinkedList<Character> operands = new LinkedList();
        for(int i = 0; i < statement.length(); i++){
            if(isDelimiter(statement.charAt(i))){
                continue;
            }else if(Character.isDigit(statement.charAt(i))){
                while(!isDelimiter(statement.charAt(i)) && !isOperator(statement.charAt(i))){
                    modifiedStatement.append(statement.charAt(i));
                    i++;
                    if(i == statement.length()){
                        break;
                    }
                }
                modifiedStatement.append(" ");
                i--;
            }else if(isOperator(statement.charAt(i))){
                if(statement.charAt(i) == '('){
                    operands.add(statement.charAt(i));
                }else if(statement.charAt(i) == ')'){
                    char operandFromList = operands.pollLast();
                    while(operandFromList != '('){
                        modifiedStatement.append(operandFromList + " ");
                        operandFromList = operands.pollLast();
                    }
                }else{
                    if(operands.size() > 0){
                        if(getPriority(statement.charAt(i)) <= getPriority(operands.peekLast())){
                            modifiedStatement.append(operands.pollLast() + " ");
                        }
                    }
                    operands.add(statement.charAt(i));
                }
            }
        }

        while (operands.size() > 0){
            modifiedStatement.append(operands.pollLast() + " ");
        }
        return modifiedStatement.toString();
    }

    /**
     *
     * @param modifiedStatement  statement in Reverse Polish notation
     * @return  result of mathematical calculation of statement in Reverse Polish notation
     * @throws Exception
     */
    private static double solveStatement(String modifiedStatement)throws Exception{
        double result = 0;
        LinkedList<Double> tempList = new LinkedList<>();
        for(int i = 0; i < modifiedStatement.length(); i++){
            if(Character.isDigit(modifiedStatement.charAt(i))){
                StringBuilder number = new StringBuilder();
                while(!isDelimiter(modifiedStatement.charAt(i)) && !isOperator(modifiedStatement.charAt(i))){
                    number.append(modifiedStatement.charAt(i));
                    i++;
                    if(i == modifiedStatement.length()) break;
                }
                tempList.add(Double.parseDouble(number.toString()));
                i--;
            }else if(isOperator(modifiedStatement.charAt(i))){
                double num1 = tempList.pollLast();
                double num2 = tempList.pollLast();
                result = doOperation(modifiedStatement.charAt(i), num2, num1);
                tempList.add(result);
            }
        }
        return tempList.peek();
    }

    /**
     * method does mathematical calculations
     * @param i operator
     * @param num2 operand 2
     * @param num1 operand 1
     * @return  result of mathematical calculations
     * @throws Exception
     */
    private static double doOperation(char i, double num2, double num1)throws Exception{
        if(i == '/' && num1 == 0){
            throw new ArithmeticException("Divided by zero");
        }
        switch (i){
            case '-': return num2 - num1;
            case '+': return num2 + num1;
            case '*': return num2 * num1;
            case '/': return num2 / num1;
        }
        return 0d;
    }

    /**
     *
     * @param statement
     * @return true - if statement is correct, otherwise - false
     */
    private static boolean isCorrect(String statement){
        if(statement == null || statement.equals("")){
            return false;
        }
        if(statement.length() - statement.replaceAll("\\(", "").length() !=
                statement.length() - statement.replaceAll("\\)", "").length() ){
            return false;
        }
        char arr[] = statement.toCharArray();
        int  openBracket = 0;
        for(int i = 0; i < arr.length; i++){
            if(arr[i] == '('){
                openBracket = i;
            }
            if(arr[i] == ')'){
                if(openBracket == 0){
                    return false;
                }
            }
        }
        String regex = ".+[^\\+\\-\\*\\/\\.\\(\\)0-9].+|^[\\-\\+\\*\\/\\)\\.].+|.+[\\-\\+\\*\\/\\(\\.]$|" +
                ".+[\\.][\\.].+|.+[\\.][0-9]+[\\.].+|.+[\\+\\-\\*\\/][\\+\\-\\*\\/].+|.+[\\+\\-\\*\\/][\\)].+|" +
                ".+[\\(][\\+\\-\\*\\/].+|.+[0-9][\\(].+|.+[\\)][0-9].+";
        if (Pattern.matches(regex, statement)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param doubleRes result of mathematical calculations
     * @return correct format of value of result
     */
    private static String getResult(double doubleRes){
        String str = String.valueOf(doubleRes);
        int index = str.lastIndexOf(".");
        char[] arr;
        if(str.length() >= index + 5){
            arr = str.substring(0,index+5).toCharArray();
        }else{
            arr = str.toCharArray();
        }
        int count = 0;
        for(int i = index + 1; i < arr.length; i++){
            if(arr[i] != '0'){
                count ++;
            }
        }
        if(count == 0){
            return  str.substring(0, index);
        }
        return  str.substring(0, index + count + 1 );
    }


}
