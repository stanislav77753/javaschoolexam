package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int yElement = 0;
        if ((x != null) && (y != null)) {
            for (int xElement = 0; xElement < x.size(); xElement++) {
                if (yElement != -1) {
                    yElement = findXElementInY(xElement, x, yElement, y);
                }
                else {
                    break;
                }
            }
        }
        else {
            throw new IllegalArgumentException();
        }
        return (yElement != -1);
    }

    /**
     *
     * @param xElement elemetn of X sequence
     * @param x X sequence
     * @param yElement elemetn of Y sequence
     * @param y Y sequence
     * @return index xElement in Y sequence, if doesn't find then return -1
     */
    private static int findXElementInY(int xElement, List x, int yElement, List y) {
        for (int i = yElement; i < y.size(); i++) {
            if (x.get(xElement).equals(y.get(i))) {
                return i;
            }
        }
        return -1;
    }
}
