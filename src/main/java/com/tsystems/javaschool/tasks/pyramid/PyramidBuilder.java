package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] result;

        if(checkRootEquation(getRootEquation(inputNumbers.size())) && (!inputNumbers.contains(null))){
            int rowNumbers = (int)getRootEquation(inputNumbers.size());
            int ccolumnNumbers = 2 * rowNumbers - 1;
            result = setNulls(rowNumbers, ccolumnNumbers);
            Collections.sort(inputNumbers);
            int numCount = 0;
            for(int i = 0; i < rowNumbers; i++) {
                int count = 0;
                int step = 0;
                for (int j = 0; j < ccolumnNumbers; j++) {
                    if (j == ccolumnNumbers - rowNumbers - i + step && count < i + 1) {
                        result[i][j] = inputNumbers.get(numCount);
                        numCount++;
                        count++;
                        step += 2;
                    }
                }
            }
        }
        else{
            throw new CannotBuildPyramidException();
        }
        return result;
    }

    /** for to get numbers of rows need to solve quadratic equation (n^2 + n - 2*listSize = 0)
     *
     * @param listSize size of input list
     * @return root of the quadratic equation
     */
    private static double getRootEquation(int listSize){
        return (-1 + Math.sqrt(1 + 8*listSize)) / 2;
    }

    /**
     *
     * @param n numbers of rows
     * @param m numbers of columns
     * @return matrix filled of zero
     */
    private static int[][] setNulls(int n, int m){
        int[][] arr = new int[n][m];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                arr[i][j] = 0;
            }
        }
        return arr;
    }

    /**
     *
     * @param root root of the quadratic equation
     * @return true - if root is integer value, otherwise - false
     */
    private static boolean checkRootEquation(double root){
        return root % 1 == 0;
    }


}
